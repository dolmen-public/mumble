# Mumble

Build docker from official repo https://github.com/mumble-voip/mumble. 

Git clone of sources is done at build time, see [.gitlab-ci.yml](./.gitlab-ci.yml).

Images:
* 1.4 : Daily build from [1.4.x](https://github.com/mumble-voip/mumble/tree/1.4.x) branch
    * Via GitLab `docker pull registry.gitlab.com/savadenn-public/mumble/1.4`
    * Via dockerHub : `docker pull savadenn/mumble:1.4`